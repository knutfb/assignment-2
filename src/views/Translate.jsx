import withAuth from "../hoc/withAuth"
import { useState } from "react"
import { useUser } from "../context/UserContext"
import { translationAdd } from "../api/translation"
import { storageSave } from "../utils/storage"
import { STORAGE_KEY_USER } from "../const/storageKeys"
import TranslationForm from "../components/Translation/TranslationForm"
import TranslationDisplay from "../components/Translation/TranslationDisplay"
import * as React from "react"
import Box from "@mui/material/Box"
import Grid from "@mui/material/Grid"

const Translations = () => {
  const [translation, setTranslation] = useState("")
  const { user, setUser } = useUser()

  const handleTranslationCliked = async (translation) => {
    if (!translation) {
      // check if we have a translation
      alert("please make a translation")
      return
    }
    // send http-request
    const [error, updatedUser] = await translationAdd(user, translation)
    if (error !== null) {
      return
    }
    // keep UI state and server state in sync
    storageSave(STORAGE_KEY_USER, updatedUser)
    // update context-state
    setUser(updatedUser)

    // Set Translation
    setTranslation(translation)
  }

  return (
    <>
      <Box sx={{ width: "100%", backgroundColor: "#FFC75F" }}>
        <Grid
          container
          direction="row"
          justifyContent="center"
          alignItems="center"
        >
          <Grid item xs={8}>
            <TranslationForm onTranslation={handleTranslationCliked} />
            <TranslationDisplay word={translation} />
          </Grid>
        </Grid>
      </Box>
    </>
  )
}
export default withAuth(Translations)
