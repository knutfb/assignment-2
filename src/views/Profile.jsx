import { useEffect } from "react"
import { userById } from "../api/user"
import ProfileActions from "../components/Profile/ProfileActions"
import ProfileHeader from "../components/Profile/ProfileHeader"
import ProfileTranslationHistory from "../components/Profile/ProfileTranslationHistory"
import { STORAGE_KEY_USER } from "../const/storageKeys"
import { useUser } from "../context/UserContext"
import withAuth from "../hoc/withAuth"
import { storageSave } from "../utils/storage"
import Box from "@mui/material/Box"
import Grid from "@mui/material/Grid"

const Profile = () => {
  const { user, setUser } = useUser()

  useEffect(() => {
    const findUser = async () => {
      const [error, latestUser] = await userById(user.id)
      if (error === null) {
        storageSave(STORAGE_KEY_USER, latestUser)
        setUser(latestUser)
      }
    }
    findUser()
  }, [setUser, user.id])

  return (
    <>
      <Box sx={{ width: "100%", backgroundColor: "#FFC75F" }}>
        <Grid
          container
          direction="row"
          justifyContent="center"
          alignItems="center"
        >
          <Grid item xs={12}>
            <ProfileHeader username={user.username} />
          </Grid>
          <Grid item={12}>
            <Grid
              container
              direction="row"
              justifyContent="center"
              alignItems="center"
              spacing={4}
            >
              <Grid item xs={6}>
                <ProfileActions />
              </Grid>
              <Grid item xs={6}>
                <ProfileTranslationHistory translations={user.translations} />
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Box>
    </>
  )
}
export default withAuth(Profile)
