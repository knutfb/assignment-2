import LoginForm from "../components/Login/LoginForm"
import * as React from "react"
import { styled } from "@mui/material/styles"
import Box from "@mui/material/Box"
import Paper from "@mui/material/Paper"
import Grid from "@mui/material/Grid"

const Login = () => {
  return (
    <Box sx={{ width: "100%", backgroundColor: "#FFC75F" }}>
      <Grid
        container
        direction="row"
        justifyContent="center"
        alignItems="center"
      >
        <Grid item xs={8}>
          <LoginForm />
        </Grid>
      </Grid>
    </Box>
  )
}
export default Login
