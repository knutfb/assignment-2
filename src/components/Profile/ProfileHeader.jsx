const ProfileHeader = ({ username }) => {
  return (
    <header>
      <h4 style={{ marginLeft: "2rem" }}>Hello, welcome back {username}</h4>
    </header>
  )
}
export default ProfileHeader
