import { storageDelete, storageSave } from "../../utils/storage"
import { useUser } from "../../context/UserContext"
import { STORAGE_KEY_USER } from "../../const/storageKeys"
import { translationClearHistory } from "../../api/translation"
import { Button } from "@mui/material"
import Grid from "@mui/material/Grid"

const ProfileActions = () => {
  const { user, setUser } = useUser()

  const handleLogoutClick = () => {
    if (window.confirm("Are you sure?")) {
      storageDelete(STORAGE_KEY_USER)
      setUser(null)
    }
  }

  const handleClearHistoryClick = async () => {
    if (!window.confirm("Are you sure?\nThis cannot be undone")) {
      return
    }
    const [clearError] = await translationClearHistory(user.id)

    if (clearError !== null) {
      return
    }
    const updatedUser = {
      ...user,
      translations: [],
    }
    storageSave(STORAGE_KEY_USER, updatedUser)
    setUser(updatedUser)
  }

  return (
    <Grid container p={2}>
      <Grid item xs={12} mb={2}>
        <Button fullWidth variant="outlined" onClick={handleClearHistoryClick}>
          Clear history
        </Button>
      </Grid>
      <Grid item xs={12} mb={2}>
        <Button fullWidth variant="outlined" onClick={handleLogoutClick}>
          Log out
        </Button>
      </Grid>
    </Grid>
  )
}
export default ProfileActions
