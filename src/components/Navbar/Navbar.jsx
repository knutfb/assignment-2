import { NavLink, useLocation } from "react-router-dom"
import { useUser } from "../../context/UserContext"
import * as React from "react"
import AppBar from "@mui/material/AppBar"
import Toolbar from "@mui/material/Toolbar"
import Typography from "@mui/material/Typography"
import IconButton from "@mui/material/IconButton"

const Navbar = () => {
  const { user } = useUser()
  const { pathname } = useLocation()
  console.log(pathname)
  return (
    <>
      {user !== null && (
        <AppBar position="static">
          <Toolbar style={{ backgroundColor: "#E7B355" }} variant="dense">
            <IconButton edge="start" aria-label="menu" disabled sx={{ mr: 2 }}>
              <img
                style={{ maxWidth: "2rem" }}
                src={window.location.origin + "/img/resources/Logo.png"}
                alt=""
              />
            </IconButton>

            <Typography variant="h6" color="inherit" component="div">
              {pathname !== "/translations" ? (
                <NavLink style={{ textDecoration: "none" }} to="/translations">
                  Translations
                </NavLink>
              ) : (
                <NavLink style={{ textDecoration: "none" }} to="/profile">
                  Profile
                </NavLink>
              )}
            </Typography>
          </Toolbar>
        </AppBar>
      )}
    </>
  )
}
export default Navbar
