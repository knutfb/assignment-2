import { useState, useEffect } from "react"
import { useForm } from "react-hook-form"
import { loginUser } from "../../api/user"
import { storageSave } from "../../utils/storage"
import { useNavigate } from "react-router-dom"
import { useUser } from "../../context/UserContext"
import { STORAGE_KEY_USER } from "../../const/storageKeys"
import Grid from "@mui/material/Grid"

import InputBase from "@mui/material/InputBase"
import IconButton from "@mui/material/IconButton"
import Paper from "@mui/material/Paper"
import ArrowCircleRightSharpIcon from "@mui/icons-material/ArrowCircleRightSharp"

const usernameConfig = {
  required: true,
  minLength: 3,
}

const LoginForm = () => {
  // Hooks
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm()
  const { user, setUser } = useUser()
  const navigate = useNavigate()
  const submitButton = (props) => (
    <button {...props} type="submit" disabled={loading} />
  )

  //LOCAL STATE
  const [loading, setLoading] = useState(false)
  const [apiError, setApiError] = useState(null)

  //SIDE EFFECTS
  useEffect(() => {
    if (user !== null) {
      navigate("translations")
    }
    console.log("User has changed", user)
  }, [user, navigate]) //emtpty dependencies - Only runs once

  // EVENT HANDLERS

  const onSubmit = async ({ username }) => {
    console.log(username)
    setLoading(true)
    const [error, userResponse] = await loginUser(username)
    if (error !== null) {
      setApiError(error)
    }
    if (userResponse !== null) {
      storageSave(STORAGE_KEY_USER, userResponse)
      setUser(userResponse)
    }
    setLoading(false)
  }

  //RENDER FUNCTIONS
  const errorMessage = (() => {
    if (!errors.username) {
      return null
    }
    if (errors.username.type === "required") {
      return <span> Username is required </span>
    }
    if (errors.username.type === "minLength") {
      return <span> Username is too short (min 3 characters) </span>
    }
  })()

  return (
    <>
      <Grid container height={"100%"}>
        <Grid item xs={12}>
          <Grid
            container
            direction="row"
            justifyContent="center"
            alignItems="center"
            mt={2}
            mb={2}
          >
            <Grid item style={{ margin: "auto" }}>
              <img
                style={{ maxWidth: "8rem" }}
                src={window.location.origin + "/img/resources/Logo.png"}
                alt=""
              />
            </Grid>
            <Grid item style={{ marginRight: "auto" }}>
              <h2>Lost in translation</h2>
            </Grid>
          </Grid>
        </Grid>

        <Grid item height={"1rem"} xs={12}>
          <Grid
            container
            direction="row"
            justifyContent="center"
            alignItems="center"
          >
            <Grid item xs={10}>
              <Paper
                component="form"
                sx={{
                  p: "2px 4px",
                  display: "flex",
                  alignItems: "center",
                  width: "100%",
                  borderRadius: 10,
                }}
                onSubmit={handleSubmit(onSubmit)}
              >
                <InputBase
                  sx={{ ml: 1, flex: 1 }}
                  placeholder="Whats your name?"
                  type="text"
                  {...register("username", usernameConfig)}
                />

                <IconButton sx={{ p: "5px" }} component={submitButton}>
                  <ArrowCircleRightSharpIcon
                    style={{ color: "#845EC2", scale: "1.5" }}
                  />
                </IconButton>
              </Paper>
              {errorMessage}
              {loading && <p>Logging in... </p>}
              {apiError && <p>{apiError}</p>}
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </>
  )
}
export default LoginForm
