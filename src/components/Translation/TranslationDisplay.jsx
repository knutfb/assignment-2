import TranslationItem from "./TranslationItem"
import Grid from "@mui/material/Grid"

const TranslationDisplay = ({ word }) => {
  return (
    <>
      <Grid container height={"100%"}>
        <Grid item height={"1rem"}>
          {word
            .split("")
            .filter((e) => e.trim())
            .map((letter, idx) => (
              <TranslationItem key={idx} letter={letter} />
            ))}
        </Grid>
      </Grid>
    </>
  )
}
export default TranslationDisplay
