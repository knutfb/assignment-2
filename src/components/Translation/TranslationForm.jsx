import { useForm } from "react-hook-form"

import Grid from "@mui/material/Grid"
import InputBase from "@mui/material/InputBase"
import IconButton from "@mui/material/IconButton"
import Paper from "@mui/material/Paper"
import ArrowCircleRightSharpIcon from "@mui/icons-material/ArrowCircleRightSharp"

const TranslationForm = ({ onTranslation }) => {
  const { register, handleSubmit } = useForm()
  const onSubmit = ({ translation }) => {
    onTranslation(translation)
  }
  const submitButton = (props) => <button {...props} type="submit" />
  return (
    <>
      <Grid container style={{ backgroundColor: "#FFC75F" }}>
        <Grid item xs={12}>
          <Grid
            container
            direction="row"
            justifyContent="center"
            alignItems="center"
            mt={2}
            mb={2}
          >
            <Grid item style={{ margin: "auto" }}>
              <img
                style={{ maxWidth: "8rem" }}
                src={window.location.origin + "/img/resources/Logo-Hello.png"}
                alt=""
              />
            </Grid>
            <Grid item style={{ marginRight: "auto" }}>
              <h2>What do you want to translate?</h2>
            </Grid>
          </Grid>
        </Grid>

        <Grid item xs={12}>
          <Grid
            container
            direction="row"
            justifyContent="center"
            alignItems="center"
          >
            <Grid item xs={10}>
              <Paper
                component="form"
                sx={{
                  p: "2px 4px",
                  display: "flex",
                  alignItems: "center",
                  width: "100%",
                  borderRadius: 10,
                  marginBottom: 2,
                }}
                onSubmit={handleSubmit(onSubmit)}
              >
                <InputBase
                  sx={{ ml: 1, flex: 1 }}
                  placeholder="Translate here!"
                  type="text"
                  {...register("translation")}
                />

                <IconButton sx={{ p: "5px" }} component={submitButton}>
                  <ArrowCircleRightSharpIcon
                    style={{ color: "#845EC2", scale: "1.5" }}
                  />
                </IconButton>
              </Paper>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </>
  )
}
export default TranslationForm
