const TranslationItem = ({ letter }) => {
  return letter && <img src={`./img/signImages/${letter}.png`} alt={letter} />
}
export default TranslationItem
