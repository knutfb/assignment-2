## README

## Lost in Translation

This is the second assignment during the frontend section of the Noroff Accelerate program.

The program has a login-, translation- and profile page.
The program lets the user log in, and translate words into sign language! It lets the user see its lastest 10 translations in the users profile page, and here, the user is also able to clear its translation history.

## Technology

```sh
React.js, Javascript, Material.UI, Node.js
```

## Install

The project uses node and npm. Initiate the project with:

```sh
$ npm install  &  npm start
```
